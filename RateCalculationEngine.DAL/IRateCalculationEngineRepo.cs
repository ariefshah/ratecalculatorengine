﻿using RateCalculationEngine.Core;

namespace RateCalculationEngine.DAL
{
    public interface IRateCalculationEngineRepo
    {
         OutputResult RateCalculatorResult(PatronDto patron);
    }

}
