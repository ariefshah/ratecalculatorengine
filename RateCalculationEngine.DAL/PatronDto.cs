﻿using System;

namespace RateCalculationEngine.DAL
{
    public class PatronDto
    {
        public DateTime EntryDateTime { get; set; }
        public DateTime ExitDateTime { get; set; }
    }
}