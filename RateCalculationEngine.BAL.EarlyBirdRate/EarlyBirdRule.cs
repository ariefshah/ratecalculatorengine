﻿using System;
using RateCalculationEngine.Core;
namespace RateCalculationEngine.BAL.EarlyBirdRule
{
    /// <summary>
    /// Name of the Rate Early Bird
    //    Type Flat Rate
    //    Total Price $13.00
    //Entry condition Enter between 6:00 AM to 9:00 AM
    //Exit condition Exit between 3:30 PM to 11:30 PM
    /// </summary>
    public class EarlyBirdRule : IRateCalculator
    {
       
        const float rate = 13.00F;

        public bool IsSatisfied(Patron patron)
        {
            return CheckTime(patron);
        }

         bool CheckTime(Patron patron)
        {

            if (CheckEntry(patron) && CheckExit(patron))
            {
                return true;
            }


            return false;
        }

        public float TotalPrice(Patron patron)
        {
            if (!IsSatisfied(patron))
            {
                throw new Exception("Rule not satisfied");
            }
            return rate;
        }


         bool CheckEntry(Patron patron)
        {
            var _entryTimeStart = patron.EntryDateTime.GetDateTime(hours: 6);
            var _entryTimeEnd = patron.EntryDateTime.GetDateTime(hours: 9);
            var entryStartCheck = patron.EntryDateTime.TimeOfDay >= _entryTimeStart.TimeOfDay;
            var entryEndCheck = patron.EntryDateTime.TimeOfDay <= _entryTimeEnd.TimeOfDay;
            return (entryStartCheck && entryEndCheck);
        }

         bool CheckExit(Patron patron)
        {
            var _exitTimeStart = patron.ExitDateTime.GetDateTime(hours: 15, minutes: 30);
            var _exitTimeEnd = patron.ExitDateTime.GetDateTime(hours: 23, minutes: 30);
            var exitStartCheck = patron.ExitDateTime.TimeOfDay >= _exitTimeStart.TimeOfDay;
            var exitEndCheck = patron.ExitDateTime.TimeOfDay <= _exitTimeEnd.TimeOfDay;
            return exitStartCheck && exitEndCheck;

        }

        public string RuleName(Patron patron)
        {
            return "Early Bird";
        }
    }
}