﻿using System;
using System.Collections.Generic;
using System.Text;
using RateCalculationEngine.Core;
namespace RateCalculationEngine.BAL.WeekendRate
{
    /// <summary>
    /// Name of the Rate Weekend Rate
    //    Type Flat Rate
    //    Total Price $10.00
    //Entry condition Enter anytime past midnight on Friday to Sunday
    //Exit condition Exit any time before midnight of Sunday
    //Note: If a patron enters the carpark before midnight on Friday and if they qualify
    //for Night rate on a Saturday morning, then the program should charge the
    // night rate instead of weekend rate.
    /// </summary>
    public class WeekendRateRule : IRateCalculator
    {
        
        private const float rate = 10.0f;
       
        public bool IsSatisfied(Patron patron)
        {
            if (CheckEntry(patron) && CheckExit(patron))
            {
                return true;
            }
            return false;
        }
        public string RuleName(Patron patron)
        {
            return "Weekend Rate";
        }

        private bool CheckEntry(Patron patron)
        {
            var Entryday = patron.EntryDateTime.DayOfWeek;

            var _entryTimeStart = patron.EntryDateTime.GetDateTime(hours: 00);
            var _entryTimeEnd = patron.EntryDateTime.GetDateTime(hours: 48);

            ///Rule
            ///
            var dayRuleCheck = (Entryday == DayOfWeek.Saturday || 
                            Entryday == DayOfWeek.Sunday);

            var entryStartCheck = patron.EntryDateTime >= _entryTimeStart;
            var entryEndCheck = patron.EntryDateTime <= _entryTimeEnd;
            return (dayRuleCheck && entryStartCheck && entryEndCheck);
        }
        private bool CheckExit(Patron patron)
        {
            var Exitday = patron.EntryDateTime.DayOfWeek;


            var _exitTimeStart = patron.EntryDateTime.GetDateTime(hours: 00);
            var _exitTimeEnd = patron.EntryDateTime.GetDateTime( hours: 48);

            ///Rule
            ///
            var dayRuleCheck = (Exitday == DayOfWeek.Saturday ||
                          Exitday == DayOfWeek.Sunday);

            var exitStartCheck = patron.ExitDateTime >= _exitTimeStart;
            var exitEndCheck = patron.ExitDateTime <= _exitTimeEnd;

            return dayRuleCheck && exitStartCheck && exitEndCheck;
        }
        public float TotalPrice(Patron patron)
        {
            if (!IsSatisfied(patron))
            {
                throw new Exception("Rule Not Satisfied");
            }
            return rate;
        }
    }
}
