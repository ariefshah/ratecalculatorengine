import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './home/welcome.component';
import { PatronComponent } from './patron/patron.component';
import { CalendarModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    BrowserModule, FormsModule,
    HttpModule,
    CalendarModule,
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule.forRoot([
      { path: 'welcome', component: WelcomeComponent },
      { path: 'patron', component: PatronComponent },
      { path: '', redirectTo: 'welcome', pathMatch: 'full' },
      { path: '**', redirectTo: 'welcome', pathMatch: 'full' }
    ])
  ],
  declarations: [
    AppComponent,
    WelcomeComponent,
    PatronComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
