"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var moment = require("moment/moment");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/do");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
require("rxjs/add/observable/throw");
var PatronService = (function () {
    // private url = 'http://localhost:54518/api/ratecalculation';   //kestrol
    function PatronService(_http) {
        this._http = _http;
        // private url = 'http://localhost:57643/api/ratecalculation'; // mac
        this.url = 'http://localhost:54517/api/ratecalculation'; //IIS
    }
    PatronService.prototype.postPatron = function (patron) {
        console.log('Patron:', patron);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var patronDto = {
            EntryDateTime: this.formatDate(patron.entryDate),
            ExitDateTime: this.formatDate(patron.exitDate)
        };
        var options = new http_1.RequestOptions({ headers: headers });
        var body = JSON.stringify(patronDto);
        console.log('result:', body);
        return this._http.post(this.url, body, options)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return Observable_1.Observable.throw(error.json().error || 'Server error: Please check if server is up and running'); });
        // .catch(this.handleErrorObservable);
    };
    PatronService.prototype.handleErrorObservable = function (error) {
        console.error(error.message || error);
        return Observable_1.Observable.throw(error.message || error);
    };
    PatronService.prototype.formatDate = function (dateVar) {
        return moment(dateVar).format('MM-DD-YYYY HH:mm:ss');
    };
    ;
    return PatronService;
}());
PatronService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PatronService);
exports.PatronService = PatronService;
//# sourceMappingURL=patron.service.js.map