import { OnInit } from '@angular/core/core';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CalendarModule } from 'primeng/primeng';

import { IPatron } from './patron';
import { PatronService } from './patron.service';

@Component({
    templateUrl: 'app/patron/patron.component.html',
    styleUrls: ['app/patron/patron.component.css'],
    providers: [PatronService, CalendarModule],
})
export class PatronComponent implements OnInit {
    pageTitle: string = 'Patron Exit entry Detail';
    Patron: IPatron;
    errorMessage: string;
    Result: any;
    constructor(private _patronService: PatronService, private _route: ActivatedRoute,
        private _router: Router, private _calendarModule: CalendarModule) {

    }

    ngOnInit() {
        this.Patron = {
            entryDate: new Date(Date.now()),
            exitDate: new Date(Date.now()),
        };
    }
    onSubmit(): void {
        //  this._patronService.postPatron(this.Patron);
        this._patronService.postPatron(this.Patron)
            .subscribe(data => {
                this.Result = data;
            },
            error => {
                this.errorMessage = <any>error;
            });
    }

}
