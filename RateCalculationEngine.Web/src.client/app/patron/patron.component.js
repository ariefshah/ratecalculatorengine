"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var primeng_1 = require("primeng/primeng");
var patron_service_1 = require("./patron.service");
var PatronComponent = (function () {
    function PatronComponent(_patronService, _route, _router, _calendarModule) {
        this._patronService = _patronService;
        this._route = _route;
        this._router = _router;
        this._calendarModule = _calendarModule;
        this.pageTitle = 'Patron Exit entry Detail';
    }
    PatronComponent.prototype.ngOnInit = function () {
        this.Patron = {
            entryDate: new Date(Date.now()),
            exitDate: new Date(Date.now()),
        };
    };
    PatronComponent.prototype.onSubmit = function () {
        var _this = this;
        //  this._patronService.postPatron(this.Patron);
        this._patronService.postPatron(this.Patron)
            .subscribe(function (data) {
            _this.Result = data;
        }, function (error) {
            _this.errorMessage = error;
        });
    };
    return PatronComponent;
}());
PatronComponent = __decorate([
    core_1.Component({
        templateUrl: 'app/patron/patron.component.html',
        styleUrls: ['app/patron/patron.component.css'],
        providers: [patron_service_1.PatronService, primeng_1.CalendarModule],
    }),
    __metadata("design:paramtypes", [patron_service_1.PatronService, router_1.ActivatedRoute,
        router_1.Router, primeng_1.CalendarModule])
], PatronComponent);
exports.PatronComponent = PatronComponent;
//# sourceMappingURL=patron.component.js.map