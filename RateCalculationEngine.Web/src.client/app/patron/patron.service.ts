import { RequestMethod } from '@angular/http/http';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import * as moment from 'moment/moment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { IPatron } from './patron';

@Injectable()
export class PatronService {
   // private url = 'http://localhost:57643/api/ratecalculation'; // mac
     private url = 'http://localhost:54517/api/ratecalculation'; //IIS
    // private url = 'http://localhost:54518/api/ratecalculation';   //kestrol
    constructor(private _http: Http) { }

    postPatron(patron: IPatron): Observable<IPatron> {
        console.log('Patron:', patron);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let patronDto = {
            EntryDateTime: this.formatDate(patron.entryDate),
            ExitDateTime: this.formatDate(patron.exitDate)
        };

        let options = new RequestOptions({ headers: headers });

        let body = JSON.stringify(patronDto);
        console.log('result:', body);
        return this._http.post(this.url, body, options)
            .map(res => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error: Please check if server is up and running'));
        // .catch(this.handleErrorObservable);
    }
    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }
    private formatDate(dateVar: Date) {
        return moment(dateVar).format('MM-DD-YYYY HH:mm:ss')
    };
}