﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using RateCalculationEngine.DAL;

namespace RateCalculationEngine.Web.Controllers
{
  //  [Produces("application/json")]
    [Route("api/[controller]")]
   public class RateCalculationController : Controller
    {
        readonly IRateCalculationEngineRepo _repo;

        public RateCalculationController(IRateCalculationEngineRepo repo)
        {
            _repo = repo;
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "API", "Working" };
        }

        // POST api/values
        [HttpPost]
        [EnableCors("MyPolicy")]
        public IActionResult Post([FromBody]PatronDto patronDto)
        {
            var result = _repo.RateCalculatorResult(patronDto);

            return Ok(result);
        }

    }
}
