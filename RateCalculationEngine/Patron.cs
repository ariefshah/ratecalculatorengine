﻿using System;
namespace RateCalculationEngine.Core
{
    public class Patron
    {
        public DateTime EntryDateTime { get; set; }
        public DateTime ExitDateTime { get; set; }
    }
}