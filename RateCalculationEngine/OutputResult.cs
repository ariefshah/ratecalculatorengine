﻿namespace RateCalculationEngine.Core
{
    public class OutputResult
    {
        public float TotalPrice { get; set; }
        public string RuleName { get; set; }
    }
}