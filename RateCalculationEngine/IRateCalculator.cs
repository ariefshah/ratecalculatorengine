﻿namespace RateCalculationEngine.Core
{
    public interface IRateCalculator
    {
        bool IsSatisfied(Patron patron);
        float TotalPrice(Patron patron);
        string RuleName(Patron patron);
    }
}
