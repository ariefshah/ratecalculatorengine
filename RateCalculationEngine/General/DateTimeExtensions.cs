﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RateCalculationEngine.Core
{
    public static class DateTimeExtensions
    {
        public static DateTime GetDateTime(this DateTime date, int days=0, int hours=0, int minutes=0)
        {
            var calcDate = date.Date.AddDays(days).AddHours(hours).AddMinutes(minutes);
            return calcDate;
        } 
    }
}
