﻿using RateCalculationEngine.Core;
using RateCalculationEngine.BAL.EarlyBirdRule;
using RateCalculationEngine.BAL.WeekendRate;
using RateCalculationEngine.BAL.NightRate;
using RateCalculationEngine.BAL.StandardRate;
using System.Collections.Generic;
using System;
using RateCalculationEngine.DAL;
using AutoMapper;

namespace RateCalculator.Infrastructure
{
    public class RateCalculator : IRateCalculationEngineRepo
    {
        List<IRateCalculator> _rateCalculatorRules = new List<IRateCalculator>();
        public RateCalculator()
        {
            
            _rateCalculatorRules.Add(new WeekendRateRule());
            _rateCalculatorRules.Add(new EarlyBirdRule());
            _rateCalculatorRules.Add(new NightRateRule());
            _rateCalculatorRules.Add(new StandardRateRule());
           
        }

        public OutputResult RateCalculatorResult(PatronDto patronDto)
        {
            var output = new OutputResult();
            var patron= Mapper.Map<Patron>(patronDto);
            foreach (var rateCalculatorRule in _rateCalculatorRules)
            {
                if (rateCalculatorRule.IsSatisfied(patron))
                {
                    output.TotalPrice= rateCalculatorRule.TotalPrice(patron);
                    output.RuleName = rateCalculatorRule.RuleName(patron);
                    return output;
                   
                }
            }
            throw new Exception("Cannot Calculate Rate");

        }
    }
}
