using RateCalculationEngine.BAL.EarlyBirdRule;
using System;
using Xunit;
using RateCalculationEngine.Core;

namespace RateCalculator.BAL.Test
{
    [Trait("EarlyBirdRuleTest","")]
    public class EarlyBirdRuleTest
    {
        //    Type Flat Rate
        //    Total Price $13.00
        //Entry condition Enter between 6:00 AM to 9:00 AM
        //Exit condition Exit between 3:30 PM to 11:30 PM
        [Fact]
        public void EarlyBirdTest_SatisfiedTrue_StartTimeAfter6AM_and_Before9AM_EndTimeAfter330pm_and_before1130PM()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 7, 35, 0),
                ExitDateTime = new DateTime(2017, 1, 1, 22, 15, 0)
            };


            //act
            var rule = new EarlyBirdRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), true);
            Assert.Equal(rule.TotalPrice(patron), 13.00F);
        }
        [Fact]
        public void EarlyBirdTest_SatisfiedFalse_StartTimeBefore6AM()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 5, 35, 0),
                ExitDateTime = new DateTime(2017, 1, 1, 22, 15, 0)
            };


            //act
            var rule = new EarlyBirdRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }
        [Fact]
        public void EarlyBirdTest_SatisfiedFalse_StartTimeAfter9AM()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 10, 35, 0),
                ExitDateTime = new DateTime(2017, 1, 1, 22, 15, 0)
            };


            //act
            var rule = new EarlyBirdRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }
        [Fact]
        public void EarlyBirdTest_SatisfiedFalse_EndTimeBefore330PM()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 7, 35, 0),
                ExitDateTime = new DateTime(2017, 1, 1, 15, 15, 0)
            };


            //act
            var rule = new EarlyBirdRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }

        [Fact]
        public void EarlyBirdTest_SatisfiedFalse_EndTimeAfter1130PM()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 7, 35, 0),
                ExitDateTime = new DateTime(2017, 1, 1, 23, 55, 0)
            };


            //act
            var rule = new EarlyBirdRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }
    }
}
