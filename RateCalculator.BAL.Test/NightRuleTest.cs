using RateCalculationEngine.BAL.NightRate;
using System;
using Xunit;
using RateCalculationEngine.Core;

namespace RateCalculator.BAL.Test
{
    [Trait("NightRuleTest", "")]
    public class NightRuleTest
    {
        //        Name of the Rate Night Rate
        //Type Flat Rate
        //Total Price $6.50
        //Entry condition Enter between 6:00 PM to midnight(weekdays)
        //Exit condition Exit before 6 AM the following day
        [Fact]
        public void NightRateRuleTest_SatisfiedTrue_StartTimeAfter6PM_and_Before6AM_NextDay()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 18, 35, 0),
                ExitDateTime = new DateTime(2017, 1, 2, 5, 15, 0)
            };


            //act
            var rule = new NightRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), true);
            Assert.Equal(rule.TotalPrice(patron), 6.5F);
        }
        [Fact]
        public void NightRateRuleTest_SatisfiedFalse_StartTimeBefore6PM()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 17, 35, 0),
                ExitDateTime = new DateTime(2017, 1, 1, 22, 15, 0)
            };


            //act
            var rule = new NightRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }
        [Fact]
        public void NightRateRuleTest_SatisfiedFalse_EntryAfter9PM_ExitAfter_6AM_NextDay()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 21, 35, 0),
                ExitDateTime = new DateTime(2017, 1, 2, 6, 15, 0)
            };


            //act
            var rule = new NightRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }
        [Fact]
        public void NightRateRuleTest_SatisfiedFalse_EntryBefore9PM_ExitBeforeMidnight()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 1, 1, 20, 55, 0),
                ExitDateTime = new DateTime(2017, 1, 1, 23, 15, 0)
            };


            //act
            var rule = new NightRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }

    }
}
