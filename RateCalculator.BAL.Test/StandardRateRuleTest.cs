using RateCalculationEngine.BAL.StandardRate;
using RateCalculationEngine.Core;
using System;
using Xunit;
using RateCalculationEngine.BAL.WeekendRate;

namespace RateCalculator.BAL.Test
{
    [Trait("StandardRateRuleTest", "")]
    public class StandardRateRuleTest
    {
        /// <summary>
        /// Name of the Rate Weekend Rate
        //        Name of the Rate Standard Rate
        //Type Hourly Rate
        //0 � 1 hours $5.00
        //1 � 2 hours $10.00
        //2 � 3 hours $15.00
        //3 + hours $20.00 flat rate per day for each day of parking
        /// </summary>
        ///  [Fact]
        public void StandardRateRuleTest_Entry1110_Exit1200_sameday_Charge_5f()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 5, 11, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 5, 12, 00, 0)
            };


            //act
            var rule = new StandardRateRule();
            //assert

            Assert.Equal(rule.TotalPrice(patron), 5f);
        }

        public void StandardRateRuleTest_Entry1010_Exit1200_sameday_Charge_10f()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 5, 10, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 5, 12, 00, 0)
            };


            //act
            var rule = new StandardRateRule();
            //assert

            Assert.Equal(rule.TotalPrice(patron), 10f);
        }
        [Fact]
        public void StandardRateRuleTest_Entry1010_Exit1255_sameday_Charge_15f()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 5, 10, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 5, 12, 55, 0)
            };


            //act
            var rule = new StandardRateRule();
            //assert
          
            Assert.Equal(rule.TotalPrice(patron), 15f);
        }
        [Fact]
        public void StandardRateRuleTest_Entry810_Exit1255_sameday_Charge_fullday()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 5, 8, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 5, 12, 55, 0)
            };


            //act
            var rule = new StandardRateRule();
            //assert

            Assert.Equal(rule.TotalPrice(patron), 20f);
        }

        [Fact]
        public void StandardRateRuleTest_Entry810_Exit1855_sameday_Charge_fullday()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 5, 8, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 5, 18, 55, 0)
            };


            //act
            var rule = new StandardRateRule();
            //assert

            Assert.Equal(rule.TotalPrice(patron), 20f);
        }
        [Fact]
        public void StandardRateRuleTest_Entry810_Exit1855_secondday_Charge_2_fulldays()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 5, 8, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 6, 18, 55, 0)
            };


            //act
            var rule = new StandardRateRule();
            //assert

            Assert.Equal(rule.TotalPrice(patron), 40f);
        }

        [Fact]
        public void StandardRateRuleTest_Entry810_Exit1855_thirdday_Charge_3_fulldays()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 5, 8, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 7, 18, 55, 0)
            };


            //act
            var rule = new StandardRateRule();
            //assert

            Assert.Equal(rule.TotalPrice(patron), 60f);
        }
    }
}
