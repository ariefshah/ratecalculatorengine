using RateCalculationEngine.Core;
using System;
using Xunit;
using RateCalculationEngine.BAL.WeekendRate;

namespace RateCalculator.BAL.Test
{
    [Trait("WeekendRateRuleTest", "")]
    public class WeekendRateRuleTest
    {
        /// <summary>
        /// Name of the Rate Weekend Rate
        //    Type Flat Rate
        //    Total Price $10.00
        //Entry condition Enter anytime past midnight on Friday to Sunday
        //Exit condition Exit any time before midnight of Sunday
        //Note: If a patron enters the carpark before midnight on Friday and if they qualify
        //for Night rate on a Saturday morning, then the program should charge the
        // night rate instead of weekend rate.
        /// </summary>
        [Fact]
        public void WeekendRateRuleTest_SatisfiedTrue_StartTime_Saturday_AfterMidnight_and_Exit_Before_SundayMidnight()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 3, 00, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 4, 23, 55, 0)
            };


            //act
            var rule = new WeekendRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), true);
            Assert.Equal(rule.TotalPrice(patron), 10F);
        }
        [Fact]
        public void WeekendRateRuleTest_SatisfiedFalse_StartTime_Friday_AfterMidnight_and_Exit_Before_SundayMidnight()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 1, 00, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 4, 23, 55, 0)
            };


            //act
            var rule = new WeekendRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }
        [Fact]
        public void WeekendRateRuleTest_SatisfiedFalse_StartTime_Saturday_AfterMidnight_and_Exit_Before_MondayMidnight()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 3, 00, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 5, 23, 55, 0)
            };


            //act
            var rule = new WeekendRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }
        [Fact]
        public void WeekendRateRuleTest_SatisfiedFalse_StartTime_WeekDays_AfterMidnight_and_Exit_Before_Midnight()
        {
            //arrange
            var patron = new Patron()
            {
                EntryDateTime = new DateTime(2017, 6, 6, 00, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 7, 23, 55, 0)
            };


            //act
            var rule = new WeekendRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), false);
        }
        [Fact]
        public void WeekendRateRuleTest_SatisfiedTrue_StartTime_Saturday_AfterMidnight_and_Exit_SameDay()
        {
            //arrange
            var patron = new Patron
            {
                EntryDateTime = new DateTime(2017, 6, 3, 00, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 3, 23, 55, 0)
            };


            //act
            var rule = new WeekendRateRule();
            //assert
            Assert.Equal(rule.IsSatisfied(patron), true);
            Assert.Equal(rule.TotalPrice(patron), 10F);
        }
    }
}
