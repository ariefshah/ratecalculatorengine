using RateCalculationEngine.Core;
using System;
using Xunit;
using RateCalculator.Infrastructure;
using RateCalculationEngine.DAL;

namespace RateCalculator.BAL.Test
{
    public class MappingsFixture
    {
        public MappingsFixture()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Patron, PatronDto>();
                cfg.CreateMap<PatronDto, Patron>();
            });
        }
    }
    [Trait("RateCalculatorTest", "")]
    public class RateCalculatorTest:IClassFixture<MappingsFixture>
    {
        
        [Fact]
        public void RateCalculatorTest_WeekendRate_StartTime_Saturday_AfterMidnight_and_Exit_Before_SundayMidnight()
        {
            //arrange
            var patron = new PatronDto()
            {
                EntryDateTime = new DateTime(2017, 6, 3, 00, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 4, 23, 55, 0)
            };


            //act
            var rule = new Infrastructure.RateCalculator();
            //expected
            var expectedOutput = new OutputResult()
            {
                RuleName = "Weekend Rate",
                TotalPrice = 10.0f
            };
            var actualOutput = rule.RateCalculatorResult(patron);
            //assert
            Assert.Equal(actualOutput.TotalPrice, expectedOutput.TotalPrice);
            Assert.Equal(actualOutput.RuleName, expectedOutput.RuleName);

        }

        [Fact]
        public void RateCalculatorTest_EarlyBirdRate_StartTime_WeekDay_Entry810AM_and_Exit_11PM()
        {
            //arrange
            var patron = new PatronDto()
            {
                EntryDateTime = new DateTime(2017, 6, 1, 8, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 1, 23, 00, 0)
            };


            //act
            var rule = new Infrastructure.RateCalculator();
            //expected
            var expectedOutput = new OutputResult()
            {
                RuleName = "Early Bird",
                TotalPrice = 13.0f
            };
            var actualOutput = rule.RateCalculatorResult(patron);
            //assert
            Assert.Equal(actualOutput.TotalPrice, expectedOutput.TotalPrice);
            Assert.Equal(actualOutput.RuleName, expectedOutput.RuleName);

        }

        [Fact]
        public void RateCalculatorTest_NightRate_StartTime_WeekDay_Entry630PM_and_Exit_05AM()
        {
            //arrange
            var patron = new PatronDto()
            {
                EntryDateTime = new DateTime(2017, 6, 1, 18, 30, 0),
                ExitDateTime = new DateTime(2017, 6, 2, 5, 00, 0)
            };

            //act
            var rule = new Infrastructure.RateCalculator();
            //expected
            var expectedOutput = new OutputResult()
            {
                RuleName = "Night Rate",
                TotalPrice = 6.5f
            };
            var actualOutput = rule.RateCalculatorResult(patron);
            //assert
            Assert.Equal(actualOutput.TotalPrice, expectedOutput.TotalPrice);
            Assert.Equal(actualOutput.RuleName, expectedOutput.RuleName);

        }

        [Fact]
        public void RateCalculatorTest_WeekendRateFail_NightRate_StartTime_WeekDay_Entry630PM_and_Exit_05AM()
        {
            //arrange
            var patron = new PatronDto()
            {
                EntryDateTime = new DateTime(2017, 6, 2, 23, 30, 0), //Weekend
                ExitDateTime = new DateTime(2017, 6, 3, 5, 00, 0)
            };

            //act
            var rule = new Infrastructure.RateCalculator();
            //expected
            var expectedOutput = new OutputResult()
            {
                RuleName = "Night Rate", //Weekend Rate Fail fall back Night Rate
                TotalPrice = 6.5f
            };
            var actualOutput = rule.RateCalculatorResult(patron);
            //assert
            Assert.Equal(actualOutput.TotalPrice, expectedOutput.TotalPrice);
            Assert.Equal(actualOutput.RuleName, expectedOutput.RuleName);

        }


        [Fact]
        public void RateCalculatorTest_StandardRate_StartTime_WeekDay_Entry10AM_and_Exit_11AMNext()
        {
            //arrange
            var patron = new PatronDto()
            {
                EntryDateTime = new DateTime(2017, 6, 1, 10, 30, 0), //Weekday
                ExitDateTime = new DateTime(2017, 6, 2, 11, 00, 0)
            };

            //act
            var rule = new Infrastructure.RateCalculator();
            //expected
            var expectedOutput = new OutputResult()
            {
                RuleName = "Standard Rate", 
                TotalPrice = 40f
            };
            var actualOutput = rule.RateCalculatorResult(patron);
            //assert
            Assert.Equal(actualOutput.TotalPrice, expectedOutput.TotalPrice);
            Assert.Equal(actualOutput.RuleName, expectedOutput.RuleName);

        }

        [Fact]
        public void RateCalculatorTest_EarlyBirdRateFail_StandardRate_StartTime_WeekDay_Entry810AM_and_Exit_11PM()
        {
            //arrange
            var patron = new PatronDto()
            {
                EntryDateTime = new DateTime(2017, 6, 1, 8, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 1, 23, 45, 0)
            };


            //act
            var rule = new Infrastructure.RateCalculator();
            //expected
            var expectedOutput = new OutputResult()
            {
                RuleName = "Standard Rate",
                TotalPrice = 20.0f
            };
            var actualOutput = rule.RateCalculatorResult(patron);
            //assert
            Assert.Equal(actualOutput.TotalPrice, expectedOutput.TotalPrice);
            Assert.Equal(actualOutput.RuleName, expectedOutput.RuleName);

        }

        [Fact]
        public void RateCalculatorTest_EarlyBirdRate_StartTime_WeekDay_Entry810AM_and_Exit_1130PM_BoundaryCase()
        {
            //arrange
            var patron = new PatronDto()
            {
                EntryDateTime = new DateTime(2017, 6, 1, 8, 10, 0),
                ExitDateTime = new DateTime(2017, 6, 1, 23, 30, 0)
            };


            //act
            var rule = new Infrastructure.RateCalculator();
            //expected
            var expectedOutput = new OutputResult()
            {
                RuleName = "Early Bird",
                TotalPrice = 13.0f
            };
            var actualOutput = rule.RateCalculatorResult(patron);
            //assert
            Assert.Equal(actualOutput.TotalPrice, expectedOutput.TotalPrice);
            Assert.Equal(actualOutput.RuleName, expectedOutput.RuleName);

        }
    }
}
