﻿using System;
using System.Collections.Generic;
using System.Text;
using RateCalculationEngine.Core;
namespace RateCalculationEngine.BAL.StandardRate
{
    /// <summary>
    /// Name of the Rate Standard Rate
    //    Type Hourly Rate
    //0 – 1 hours $5.00
    //1 – 2 hours $10.00
    //2 – 3 hours $15.00
    //3 + hours $20.00 flat rate per day for each day of parking
    /// </summary>
    public class StandardRateRule : IRateCalculator
    {
        const float rate_0_to_1_hour = 5.00f;
        const float rate_1_to_2_hour = 10.00f;
        const float rate_2_to_3_hour = 15.00f;
        const float rate_3_above_hour = 20.00f;

       
        public bool IsSatisfied(Patron patron)
        {
            //Catch All rule
            return true;
        }
        public string RuleName(Patron patron)
        {
            return "Standard Rate";
        }
        public float TotalPrice(Patron patron)
        {
            var timespan = patron.ExitDateTime - patron.EntryDateTime;
            var hours = timespan.Hours;
            var days = timespan.Days;
            if (hours<0)
            {
                throw new Exception("Wrong Input");
            }
            if (timespan.Minutes>0)
            {
                hours++;
            }
            if (days>0 && hours>0)
            {
                return ++days * rate_3_above_hour;
            }
            else if (days>0)
            {
                return days * rate_3_above_hour;
            }
            if (hours<=1)
            {
                return rate_0_to_1_hour;
            }
            else if (hours > 1 && hours <= 2)
            {
                return rate_1_to_2_hour;
            }
            else if (hours > 2 && hours <= 3)
            {
                return rate_2_to_3_hour;
            }
            else if (hours > 3)
            {
               
                return ++days * rate_3_above_hour;
            }

            throw new Exception("Cannot Calculate Rate");
        }
    }
}
