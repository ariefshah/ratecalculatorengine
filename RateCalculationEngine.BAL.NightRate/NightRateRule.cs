﻿using System;
using RateCalculationEngine.Core;

namespace RateCalculationEngine.BAL.NightRate
{
    /// <summary>
    /// Name of the Rate Night Rate
    //    Name of the Rate Night Rate
    //Type Flat Rate
    //Total Price $6.50
    //Entry condition Enter between 6:00 PM to midnight(weekdays)
    //Exit condition Exit before 6 AM the following day
    /// </summary>
    public class NightRateRule : IRateCalculator
    {
        const float rate = 6.5F;
       
        public bool IsSatisfied(Patron patron)
        {
            if (CheckEntry(patron) && CheckExit(patron))
            {
                return true;
            }


            return false;
        }
        public string RuleName(Patron patron)
        {
            return "Night Rate";
        }
        public float TotalPrice(Patron patron)
        {
            if (!IsSatisfied(patron))
            {
                throw new Exception("Rule not satisfied");
            }
            return rate;
           
        }
        private bool CheckEntry(Patron patron)
        {
            var _entryTimeStart = patron.EntryDateTime.GetDateTime(hours: 18);
            var _entryTimeEnd = patron.EntryDateTime.GetDateTime(hours:24);
            var entryStartCheck = patron.EntryDateTime >= _entryTimeStart;
            var entryEndCheck = patron.EntryDateTime <= _entryTimeEnd;
            return (entryStartCheck && entryEndCheck);
        }

        private bool CheckExit(Patron patron)
        {
            var _exitTimeStart = patron.EntryDateTime.GetDateTime(hours: 24);
            var _exitTimeEnd = patron.EntryDateTime.GetDateTime(days: 1, hours: 6);
            var exitStartCheck = patron.ExitDateTime >= _exitTimeStart;
            var exitEndCheck = patron.ExitDateTime <= _exitTimeEnd;
            return exitStartCheck && exitEndCheck;
        }
    }
}
