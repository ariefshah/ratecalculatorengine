# Rate Calculator #

To download the repo please use

```bash

git clone https://ariefshah@bitbucket.org/ariefshah/ratecalculatorengine.git

```

### Purpose?? ###

Write a rate calculation engine for a carpark, the inputs for this engine are:

1. Patron’s Entry Date and Time

2. Patron’s Exit Date and Time

Based on these 2 inputs the engine program should calculate the correct rate for the patron and display the name of the rate along with the total price to the patron

### How do I get set up? ###

This application is build using**  asp.net core and angular 2 **

* Configuration

```

cd .\ratecalculatorengine
dotnet restore

cd .\ratecalculatorengine\ratecalculatorengine\src.client

npm install

npm start
```

* Dependencies :

Check url property in 

```
cd \app\patron\

patron.service.ts

 private url = 'http://localhost:57643/api/ratecalculation'; // mac
// private url = 'http://localhost:54517/api/ratecalculation'; //IIS
// private url = 'http://localhost:54518/api/ratecalculation';   //kestrol

```

This should have come from configuration file only for simplicity have kept in this file.

* Running Test Cases

Each Business Logic has associated Test cases covering Postive as well as Negative scenarios.


### Application architecture ###
![architect_calc.PNG](https://bitbucket.org/repo/BgkXKda/images/2001927748-architect_calc.PNG)
![Code Structure.PNG](https://bitbucket.org/repo/BgkXKda/images/1055934861-Code%20Structure.PNG)

### Application Explained ###

Each business rule is seperated in individual project, and each must inherit from

```

IRateCalculator

```

In case new rules gets added, add new project in

```

RateCalculationEngine.BAL

```

folder and inherit IRateCalculator then add the newly added rule in 

```

RateCalculator.Infrastructure

```

This will follow Open/Closed rule of SOLID design principal.

### TODO ###

* More Error handling 

* Integration Test